import peewee
from peewee import *

db = SqliteDatabase('scraper_results.db')


class BaseModel(Model):
    class Meta:
        database = db


class Website(BaseModel):
    url = CharField()
    name = CharField()


class URL(BaseModel):
    abbreviated_path = CharField()


class FaceResult(BaseModel):
    url = ForeignKeyField(URL, related_name='face_results')
    name = CharField()
    animal_type = CharField()


if __name__ == '__main__':
    db.connect()
    try:
        db.create_tables([Website, URL, FaceResult])
    except peewee.OperationalError:
        pass
    db.close()
