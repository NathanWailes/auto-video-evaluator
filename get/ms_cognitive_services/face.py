import time

import cognitive_face
import cognitive_face as CF

from utils import get_api_key_for


def image_has_young_female(image_url):
    """

    :param image_url:
    :return:
    """
    if not CF.Key.get():
        face_api_key = get_api_key_for('get.ms_cognitive_services', 'face')
        CF.Key.set(face_api_key)

    return_face_attributes = "age,gender"  # ,headPose,smile,facialHair,glasses

    try:
        detected_faces = CF.face.detect(image_url, face_id=False, landmarks=False, attributes=return_face_attributes)
    except cognitive_face.util.CognitiveFaceException:
        print("Error while checking this URL: %s" % image_url)
        detected_faces = None

    if not detected_faces:
        print("No faces detected.")
    else:
        for detected_face in detected_faces:
            age = detected_face['faceAttributes']['age']
            # I'm not going to use the gender detector because 1) I saw an example where it mislabeled someone as male when
            # they were clearly female, and 2) for the images I'll be feeding to this function, having a young male will
            # usually mean there will also be a young female.
            gender = detected_face['faceAttributes']['gender']
            if age < 23:  # and gender == 'female':
                print("This video DOES seem to contain a female under 23: " + image_url)
                return True
            elif age >= 23:
                print("This video doesn't seem to contain any females under 23: " + image_url)
    return False


def get_results(api_instance, options):
    start_time = time.time()
    results = api_instance.detect(options)
    wait_given(start_time)
    return results


def wait_given(start_time):
    elapsed_time = time.time() - start_time
    time_to_wait = max(3.0 - elapsed_time, 3)
    time.sleep(time_to_wait)


if __name__ == '__main__':
    image_has_young_female('http://cdn4.thumbs.motherlessmedia.com/thumbs/1CB7C57-strip.jpg?from_helper')