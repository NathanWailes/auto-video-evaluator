import os
from pkgutil import get_data
import requests
import simplejson
import time

from selenium import webdriver

pause = 2


def load_browser(dot_notation_path_to_cookies, cookie_filename, headless=False):
    cookie_file = get_data(dot_notation_path_to_cookies, cookie_filename)
    cookies = simplejson.loads(cookie_file)
    phantomjs_cookies = []
    for key, val in cookies.items():
        phantomjs_cookies.append({"name": key, "value": val, "domain": "google.com"})

    if headless:
        path_to_phantomjs = './ms_windows_modules/phantomjs-2.1.1-windows/bin/phantomjs' # change path as needed
        capabilities = webdriver.DesiredCapabilities.PHANTOMJS.copy()
        capabilities["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (X11; Linux x86_64) "
                                                             "AppleWebKit/53 (KHTML, like Gecko) "
                                                             "Chrome/15.0.87")
        browser = webdriver.PhantomJS(executable_path=path_to_phantomjs, desired_capabilities=capabilities)
    else:
        browser = webdriver.Chrome()

    browser.implicitly_wait(10)
    for cookie in phantomjs_cookies:
        browser.add_cookie(cookie)
    return browser


def download_image(image_url, save_path):
    f = open(save_path, 'wb')
    f.write(requests.get(image_url).content)
    f.close()


def create_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def scroll_to_bottom_of_page(browser):
    last_height = browser.execute_script("return document.body.scrollHeight")
    while True:
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(pause)
        new_height = browser.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height


def scroll_to_top_of_page(browser):
    browser.execute_script("window.scrollTo(0, 0)")
