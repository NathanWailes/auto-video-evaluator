import time

from pyvirtualdisplay import Display
from selenium import webdriver
import os

from get.utils import load_browser
from get.ms_cognitive_services.face import image_has_young_female

pause = 2


def main():
    print("Loading browser...")
    #display = Display().start()
    if os.name == 'nt':  # If you're on Windows...
        browser = load_browser('get.motherless', 'cookies.json', headless=True)
    else:
        browser = webdriver.Firefox()

    url_to_scrape_from = 'http://motherless.com/videos/recent'  # http://motherless.com/f/muhler42/videos
    print('Navigating to %s...' % url_to_scrape_from)
    browser.get(url_to_scrape_from)
    time.sleep(pause)

    print("Searching for URLs...")
    urls = set()
    content_inner = browser.find_element_by_class_name('content-inner')
    thumbnail_containers = content_inner.find_elements_by_class_name('thumb-container')
    for thumbnail_container in thumbnail_containers:
        image_container = thumbnail_container.find_element_by_class_name('img-container')

        # When the page first loads, only static images are loaded. These show a single frame of the video.
        # When the user hovers his mouse over the static image, a second, larger image is loaded that contains many
        # frames of the video. The URL of this second image is easy to get once you know the URL of the static image.
        static_image_tag = image_container.find_element_by_class_name('static')
        static_image_url = static_image_tag.get_attribute('src')
        filmstrip_image_url = static_image_url.replace('small', 'strip')
        urls.add(filmstrip_image_url)

    print("Determining which contain young females...")
    urls_with_young_females = set()
    for url in urls:
        print('Checking %s' % url)
        if image_has_young_female(url):
            urls_with_young_females.add(url)
        time.sleep(3.01)  # Microsoft's Face API has a rate limit of 20 requests per minute.

    if browser is not None:
        browser.quit()
    # display.stop()
    print(urls_with_young_females)


if __name__ == '__main__':
    main()
