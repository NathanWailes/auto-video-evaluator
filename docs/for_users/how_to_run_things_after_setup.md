####Motherless

#####`front_page.py`

1. Within PyCharm, right-click on front_page.py and select 'Run'.
1. If you get an error, you may need to update the configuration:
    1. Click on the dropdown in the top-right of the screen that says 'front_page' and select 
    'Edit Configurations'.
    1. In the modal that appears, edit the 'Working Directory' to be the root of the project.
