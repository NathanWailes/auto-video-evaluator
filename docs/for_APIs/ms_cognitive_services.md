
####Misc notes

 - The link to the API's 'dashboard' with the API keys and links to upgrade the rate limit can be found 
 [here](https://www.microsoft.com/cognitive-services/en-us/subscriptions).



##### Face API

 - The rate limit is 30,000 transactions per month, 20 per minute.
