import json
import os
from pkgutil import get_data


class Memoize:
    def __init__(self, f):
        self.f = f
        self.memo = {}

    def __call__(self, *args):
        if args not in self.memo:
            self.memo[args] = self.f(*args)
        return self.memo[args]


def ensure_path_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)


def export_data_to_a_json_file(json_data, filename):
    with open(filename, 'w') as outfile:
        json.dump(json_data, outfile)


def get_json_data_given_filename(package, filename):
    input_file = get_data(package, filename).decode("utf-8")
    json_data = json.loads(input_file)
    return json_data


@Memoize
def get_api_keys_for(package, api_provider_name):
    api_keys = get_json_data_given_filename(package, 'api_keys.json')
    assert(len(api_keys) > 1)
    return api_keys[api_provider_name]


@Memoize
def get_api_key_for(package, api_provider_name):
    api_keys = get_json_data_given_filename(package, 'api_keys.json')
    assert(len(api_keys) == 1)
    return api_keys[api_provider_name]
